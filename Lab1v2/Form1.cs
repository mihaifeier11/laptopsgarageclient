﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Lab1v2 {
    public partial class Form1 : Form {
        DataSet dataSet;
        SqlDataAdapter dataAdapterOffers;
        SqlDataAdapter dataAdapterLaptops;
        BindingSource bindingSourceOffers;
        BindingSource bindingSourceLaptops;
        SqlCommandBuilder commandBuilder;
        string connectionString = "Server=DESKTOP-9VOI86V\\SQLEXPRESS;Database=Laptops;Integrated Security=true;";

        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            try {
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                dataAdapterLaptops = new SqlDataAdapter("SELECT * FROM Laptopuri;", connection);
                dataAdapterOffers = new SqlDataAdapter("SELECT * FROM Oferte;", connection);
                dataSet = new DataSet();

                dataAdapterLaptops.Fill(dataSet, "Laptopuri");
                dataAdapterOffers.Fill(dataSet, "Oferte");

                commandBuilder = new SqlCommandBuilder(dataAdapterOffers);
                dataSet.Relations.Add("FK_Oferte_Laptopuri", dataSet.Tables["Laptopuri"].Columns["lid"], dataSet.Tables["Oferte"].Columns["lid"]);

                bindingSourceLaptops = new BindingSource();
                bindingSourceLaptops.DataSource = dataSet;
                bindingSourceLaptops.DataMember = "Laptopuri";

                bindingSourceOffers = new BindingSource();
                bindingSourceOffers.DataSource = bindingSourceLaptops;
                bindingSourceOffers.DataMember = "FK_Oferte_Laptopuri";



                dgvLaptops.DataSource = bindingSourceLaptops;
                dgvOffers.DataSource = bindingSourceOffers;


            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            dataAdapterOffers.Update(dataSet, "Oferte");
        }
    }
}
